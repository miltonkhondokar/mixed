<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 10/15/2016
 * Time: 7:17 PM
 */

class Calculator{

    public $number_1;
    public $number_2;
    public $result;

    public function __construct($number_1,$number_2)
    {
        $this->number_1 = $number_1;
        $this->number_2 = $number_2;
    }

    public function sum(){
        $result = $this->number_1 + $this->number_2;
        echo 'Addition is : '.$result;
    }


    public function sub(){
        $result = $this->number_1 - $this->number_2;
        echo 'Subtraction is : '.$result;
    }



    public function mul(){
        $result = $this->number_1 * $this->number_2;
        echo 'Multiplication is : '.$result;
    }


    public function div(){
        $result = $this->number_1 / $this->number_2;
        echo 'Division is : '.$result;
    }
}

$obj = new Calculator(15,5);




echo $obj->sum();

echo "</br>";
echo "</br>";
echo "<hr>";
echo "</br>";
echo "</br>";

echo $obj->sub();

echo "</br>";
echo "</br>";
echo "<hr>";
echo "</br>";
echo "</br>";

echo $obj->mul();

echo "</br>";
echo "</br>";
echo "<hr>";
echo "</br>";
echo "</br>";

echo $obj->div();

