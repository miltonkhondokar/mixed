<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 10/15/2016
 * Time: 6:08 PM
 */



class Student{
    public $name = "";
    public $age = " ";

    public function sayHellow(){
        echo 'Hello from '.$this->name. ' and my age is '.$this->age;
    }

    public function __construct($name,$age)
    {
        $this->name=$name;
        $this->age=$age;
    }

}


//$rubel = new Student();
//$rubel->name='Rubel ';
//$rubel->age='25';
//echo $rubel->name;
//$rubel->sayHellow();
//echo $rubel->age;
echo "</br>";
echo "</br>";
//var_dump($rubel);

echo "</br>";
echo "</br>";
echo "<hr>";
echo "</br>";
echo "</br>";

//$nayan = new Student();
//$nayan->name='Nayan ';
//$nayan->age='28';
//echo $nayan->name;
//$nayan->sayHellow();
//echo $nayan->age;
echo "</br>";
echo "</br>";
//var_dump($nayan);

$obj=new Student('Rubel','25');
echo "</br>";
echo "</br>";
echo $obj->sayHellow();

